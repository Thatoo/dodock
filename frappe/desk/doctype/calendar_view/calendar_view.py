# Copyright (c) 2021, Frappe Technologies and contributors
# License: MIT. See LICENSE


from frappe.model.document import Document


class CalendarView(Document):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.desk.doctype.calendar_view_status.calendar_view_status import CalendarViewStatus
		from frappe.types import DF

		all_day_field: DF.Literal[None]
		color_field: DF.Literal[None]
		daily_maximum_time: DF.Time | None
		daily_minimum_time: DF.Time | None
		display_event_end: DF.Check
		display_event_time: DF.Check
		end_date_field: DF.Literal[None]
		first_day: DF.Literal["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
		recurrence_rule_field: DF.Literal[None]
		reference_doctype: DF.Link
		secondary_status: DF.Table[CalendarViewStatus]
		secondary_status_field: DF.Literal[None]
		start_date_field: DF.Literal[None]
		status_field: DF.Literal[None]
		subject_field: DF.Literal[None]
	# end: auto-generated types

	pass
