# Copyright (c) 2023, Dokos SAS and Contributors
# For license information, please see license.txt

import frappe
from frappe.tests.utils import FrappeTestCase


class TestSummaryCard(FrappeTestCase):
	def test_basic_summary_card(self):
		sc = frappe.get_doc(
			{
				"name": "ToDo Summary Card",
				"is_standard": 0,
				"module": "Desk",
				"dt": "ToDo",
				"label": "ToDo",
				"show_liked_by_me": 0,
				"show_assigned_to_me": 0,
				"doctype": "Summary Card",
				"rows": [
					{
						"type": "Count",
						"label": "Open",
						"counter_format": "#",
						"filters_code": '[["status","=","Open"]]',
					},
					{
						"type": "Count",
						"label": "Old",
						"counter_format": "#",
						"filters_code": '[["date","<=",last_7_days]]',
					},
				],
			}
		)
		sc.insert()

		try:
			from frappe.desk.doctype.summary_card.summary_card import get_summary

			summ = get_summary(sc.name)

			self.assertEqual(summ["title"], "ToDo")
			self.assertEqual(summ["dt"], "ToDo")

			self.assertEqual(summ["sections"][0]["label"], "")
			self.assertEqual(summ["sections"][0]["type"], "Section Break")

			self.assertEqual(summ["sections"][0]["items"][0]["type"], "Count")
			self.assertEqual(summ["sections"][0]["items"][0]["label"], "Open")
			self.assertEqual(summ["sections"][0]["items"][0]["dt"], "ToDo")
			self.assertIn("count", summ["sections"][0]["items"][0]["data"])
		finally:
			sc.delete()
