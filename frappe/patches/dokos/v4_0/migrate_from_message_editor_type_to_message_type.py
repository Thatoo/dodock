import frappe


def execute():
	"""Migrate from message_editor_type to message_type"""

	if not frappe.db.has_column("Notification", "message_editor_type"):
		return
	if not frappe.db.has_column("Notification", "message_block_editor"):
		return

	docs = frappe.get_all(
		"Notification",
		fields=["name", "message_editor_type", "message_block_editor"],
		filters={"is_standard": False},
	)
	frappe.reload_doc("email", "doctype", "notification")

	for doc in docs:
		if not doc.message_editor_type:
			continue

		if doc.message_editor_type == "Block Editor":
			frappe.db.set_value(
				"Notification",
				doc.name,
				{
					"message_type": "Block Editor",
					"message": doc.message_block_editor,
				},
				update_modified=False,
			)

		if doc.message_editor_type == "HTML Editor":
			frappe.db.set_value(
				"Notification",
				doc.name,
				{
					"message_type": "HTML",
				},
				update_modified=False,
			)
