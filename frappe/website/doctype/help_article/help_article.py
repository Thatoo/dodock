# Copyright (c) 2021, Frappe and contributors
# License: MIT. See LICENSE


import frappe
from frappe import _
from frappe.rate_limiter import rate_limit
from frappe.utils import cint, is_markdown, markdown
from frappe.website.utils import get_comment_list
from frappe.website.website_generator import WebsiteGenerator
from frappe.www.list import get_list


class HelpArticle(WebsiteGenerator):
	# begin: auto-generated types
	# This code is auto-generated. Do not modify anything in this block.

	from typing import TYPE_CHECKING

	if TYPE_CHECKING:
		from frappe.types import DF

		author: DF.Data | None
		category: DF.Link
		content: DF.TextEditor
		helpful: DF.Int
		level: DF.Literal["Beginner", "Intermediate", "Expert"]
		likes: DF.Int
		login_required: DF.Check
		not_helpful: DF.Int
		published: DF.Check
		route: DF.Data | None
		title: DF.Data
	# end: auto-generated types

	def validate(self):
		self.set_route()

	def set_route(self):
		"""Set route from category and title if missing"""
		if not self.route:
			self.route = "/".join(
				[frappe.get_value("Help Category", self.category, "route"), self.scrub(self.title)]
			)

	def on_update(self):
		self.update_category()

	def clear_cache(self):
		clear_knowledge_base_cache()
		return super().clear_cache()

	def update_category(self):
		cnt = frappe.db.count("Help Article", filters={"category": self.category, "published": 1})
		cat = frappe.get_doc("Help Category", self.category)
		cat.help_articles = cnt
		cat.save()

	def get_context(self, context):
		if self.login_required and frappe.session.user == "Guest":
			frappe.throw(_("You need to be logged in to access this page"), frappe.PermissionError)

		if is_markdown(context.content):
			context.content = markdown(context.content)
		context.login_required = True
		context.category = frappe.get_doc("Help Category", self.category)
		context.level_class = get_level_class(self.level)
		context.comment_list = get_comment_list(self.doctype, self.name)
		context.show_sidebar = True
		context.sidebar_items = get_sidebar_items()
		context.parents = self.get_parents(context)

	def get_parents(self, context):
		return [
			{"route": "/kb", "title": _("Knowledge Base")},
			{"title": context.category.category_name, "route": context.category.route},
		]


def get_list_context(context=None):
	filters = dict(published=1)
	introduction = False

	category = frappe.db.get_value(
		"Help Category",
		{"route": frappe.local.path},
		["name", "category_description", "login_required"],
		as_dict=True,
	)

	if category:
		if cint(category.get("login_required")) and frappe.session.user == "Guest":
			frappe.throw(_("You need to be logged in to access this page"), frappe.PermissionError)

		filters["category"] = category.get("name")
		introduction = category.get("category_description")

	if frappe.session.user == "Guest":
		filters["login_required"] = 0

	list_context = frappe._dict(
		title=category.get("name") if category else _("Knowledge Base"),
		get_level_class=get_level_class,
		show_sidebar=True,
		sidebar_items=get_sidebar_items() if category else [],
		hide_filters=True,
		filters=filters,
		category=frappe.local.form_dict.category,
		parents=[{"route": "/kb", "title": _("Knowledge Base")}] if category else [],
		no_breadcrumbs=False,
		introduction=introduction,
		list_template="templates/includes/list/list.html" if category else None,
		row_template="website/doctype/help_article/templates/help_article_row.html"
		if category
		else "website/doctype/help_article/templates/help_article_category_row.html",
	)

	if not category:
		list_context["get_list"] = get_category_list

	return list_context


def get_level_class(level):
	if not level:
		return ""

	return {"Beginner": "green", "Intermediate": "orange", "Expert": "red"}[level]


def get_sidebar_items():
	def _get():
		conditions = ""
		if frappe.session.user == "Guest":
			conditions = " and login_required = 0"
		return frappe.db.sql(
			f"""select
				category_name as title,
				concat('/', route) as route
			from
				`tabHelp Category`
			where
				published = 1 and help_articles > 0 {conditions}
			order by
				help_articles desc""",
			as_dict=True,
		)

	return frappe.cache.get_value("knowledge_base:category_sidebar", _get)


def clear_knowledge_base_cache():
	frappe.cache.delete_value("knowledge_base:category_sidebar")
	frappe.cache.delete_value("knowledge_base:faq")


def get_category_list(doctype, txt=None, filters=None, limit_start=0, limit_page_length=20, order_by=None):
	if not filters:
		filters = []

	if filters.get("category"):
		filters["name"] = filters["category"]
		filters.pop("category")

	filters.published = 1

	return frappe.get_all(
		"Help Category",
		filters=filters,
		fields=["name", "route", "category_name", "category_description", "help_articles", "cover_image"],
	)


@frappe.whitelist(allow_guest=True)
@rate_limit(key="article", limit=5, seconds=60 * 60)
def add_feedback(article: str, helpful: str):
	field = "not_helpful" if helpful == "No" else "helpful"

	value = cint(frappe.db.get_value("Help Article", article, field))
	frappe.db.set_value("Help Article", article, field, value + 1, update_modified=False)
