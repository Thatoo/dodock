include:
  - project: "dokos/docli"
    ref: develop
    file: "/.gitlab-ci.base.yml"

stages:
  - Test
  - Build

variables:
  DOCLI_IMAGE: "registry.gitlab.com/dokos/docli:ci-develop"

tests:
  image: $DOCLI_IMAGE
  stage: Test

  rules:
    - if: $SKIP_TESTS == "true"
      when: never
    - if: $CI_COMMIT_TAG != null
      allow_failure: true  # Prevent flaky tests from breaking releases
    - when: always

  services:
    - name: mariadb:10.6
      command: ['mysqld', '--character-set-server=utf8mb4', '--collation-server=utf8mb4_unicode_ci', '--character-set-client-handshake=FALSE', '--innodb_read_only_compressed=OFF']
    - name: redis
      alias: redis_queue
    - name: redis
      alias: redis_cache
    - name: redis
      alias: redis_socketio
    - name: rnwood/smtp4dev
      alias: smtp4dev

  variables:
    MYSQL_DATABASE: "test_dodock"
    MYSQL_ROOT_PASSWORD: "test_dodock"

  before_script:
    - bench init . --ignore-exist --no-backups --skip-redis-config-generation --skip-assets --frappe-path /builds/dokos/dodock/
    - /ci-utils/setup-redis.sh v1
    - /ci-utils/patch-procfile.sh v1
    - cp -r apps/frappe/test_sites/test_site sites/
    - bench setup requirements --dev
    - bench start > bench_start.log &
    - bench build --app frappe &
    - build_pid=$!
    - bench --site test_site reinstall --yes --mariadb-root-password test_dodock
    - bench --site test_site add-to-hosts
    - wait $build_pid

  script:
    - bench --site test_site run-parallel-tests --app frappe

docker:
  stage: Build
  rules:
    - if: $CI_COMMIT_TAG != null
    - if: $BUILD_DOCKER == "true"
  image: docker:git
  services:
    - docker:dind
  variables:
    NODE_VERSION: "18.16.1"
    PYTHON_VERSION: "3.10"
  script:
    - set -e
    - wget -q https://gitlab.com/dokos/docli/-/raw/develop/compose/ci/utils/build.sh -O /tmp/build.sh
    - chmod +x /tmp/build.sh

    - export IMAGE_NAME="$(/tmp/build.sh get-image-name)"

    - echo "Building image $IMAGE_NAME"
    - git clone https://github.com/frappe/frappe_docker.git
    - cd frappe_docker
    - >
      docker build \
        --build-arg=FRAPPE_PATH=https://gitlab.com/dokos/dodock \
        --build-arg=FRAPPE_BRANCH=${FRAPPE_BRANCH:-$CI_COMMIT_REF_NAME} \
        --build-arg=PYTHON_VERSION=${PYTHON_VERSION} \
        --build-arg=NODE_VERSION=${NODE_VERSION} \
        --tag=$IMAGE_NAME \
        --file=images/custom/Containerfile .

    - echo "Logging in to registry $CI_REGISTRY"
    - /tmp/build.sh login

    - echo "Pushing image $IMAGE_NAME"
    - docker push $IMAGE_NAME

    # Push latest tag if the commit is tagged and is the latest release (across all major versions)
    - if /tmp/build.sh is-latest-release $CI_COMMIT_TAG; then
        N=$(/tmp/build.sh get-image-name latest);
        echo "Pushing image $N"; docker tag $IMAGE_NAME $N; docker push $N;
      fi
